from django.test import TestCase, Client
from django.urls import resolve
from . import views

c = Client()
class Test_Story8(TestCase) :
    def test_link_exist(self):
        response = c.get('/BookSearch/')
        self.assertEqual(response.status_code, 200)

    def test_using_func(self):
        response = resolve('/BookSearch/')
        self.assertEqual(response.func, views.story8)

    def test_using_template(self):
        response = c.get('/BookSearch/')
        self.assertTemplateUsed(response, 'index.html')
