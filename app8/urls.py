from django.urls import path
from . import views

app_name = "story8"

urlpatterns = [
    path('BookSearch/', views.story8, name = 'story8'),
]
